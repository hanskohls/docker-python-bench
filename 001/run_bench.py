import pymysql.cursors
import pymysql
import time


time.sleep(10) # Wait for Mysql to start


SQL=""" 
    INSERT INTO bench ( ione, itwo, sone, stwo ) VALUES (
        1, 2, "3", "4");
    """
 
 
conn = pymysql.connect(host="mysql", port=3306, user="benchuser", passwd="1234567890", db="bench")

def singleCommit(rows, rpc):
    cur = conn.cursor()
    start = time.time()
    for n in range(0,rows/rpc):
        conn.begin()
        for m in range(0,rpc):
            cur.execute(SQL)
        conn.commit()
    end = time.time()
    dur = end - start
    rps = rows/dur
    print("rows: " + str(rows) + ", rows per batch: " + str(rpc) + ", dur: " + str(dur) + "s, rows_per_sec: " + str(rps))

    
rows = 5000000
singleCommit(rows, 1)
singleCommit(rows, 2)
singleCommit(rows, 5)
singleCommit(rows, 10)
singleCommit(rows, 20)
singleCommit(rows, 50)
singleCommit(rows, 100)
singleCommit(rows, 500)
singleCommit(rows, 1000)