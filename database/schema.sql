CREATE DATABASE bench CHARACTER SET utf8 COLLATE utf8_bin;
CREATE USER 'benchuser'@'%' IDENTIFIED BY '1234567890';
GRANT ALL PRIVILEGES on *.* to 'benchuser'@'%';
FLUSH PRIVILEGES;

USE bench;

CREATE TABLE bench (
 id bigint not null auto_increment,
 ione int(11) not null,
 itwo int(11) not null,
 sone varchar(255) not null,
 stwo varchar(255) not null, 
 primary key (id)
);